const users = [
    {
       uid: 001,
       email: 'john@dev.com',
       personalInfo: {
          name: 'John',
          address: {
             line1: 'westwish st',
             line2: 'washmasher',
             city: 'wallas',
             state: 'WX'
          }
       }
    },
    {
       uid: 063,
       email: 'a.abken@larobe.edu.au',
       personalInfo: {
          name: 'amin',
          address: {
             line1: 'Heidelberg',
             line2: '',
             city: 'Melbourne',
             state: 'VIC'
          }
       }
    },
    {
       uid: 045,
       email: 'Linda.Paterson@gmail.com',
       personalInfo: {
          name: 'Linda',
          address: {
             line1: 'Cherry st',
             line2: 'Kangaroo Point',
             city: 'Brisbane',
             state: 'QLD'
          }
       }
    }
  ]
  

function returnUsers(users){
    const object = users.map(obj => {
    const info = {};
    info.name =  obj.personalInfo.name; 
    info.email = "'" + obj.email + "'"; 
    info.state = obj.personalInfo.address.state;
    return info;
    });return object;
};  

function tableFunction(){
    var header =  '<th>' + 'Name' + '</th>'+
     '<th>' + 'Email' + '</th>'+
     '<th>' + 'State' + '</th>'
     var elements = "";
     for (let i=0; i<returnUsers(users).length;i++){
     elements +=     '<tr>' + '<td>' + returnUsers(users)[i]['name'] + '</td>' + 
                  '<td>' + returnUsers(users)[i]['email'] + '</td>' + 
                  '<td>' + returnUsers(users)[i]['state'] + '</td>' +
         '</tr>'
 }
 document.getElementById("tab").innerHTML = header + elements;
};